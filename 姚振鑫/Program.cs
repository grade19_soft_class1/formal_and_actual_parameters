﻿using System;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        { 
            int val = 0;
            RefMethod(ref val);
            int nal;
            OutMethod(out nal);
            Console.WriteLine(val);
            Console.WriteLine(nal);
        }
        static void RefMethod(ref int i)
        {
            i = 44;
           
        }
        static void OutMethod(out int a ) {
            a = 22;
        }

    }
}
