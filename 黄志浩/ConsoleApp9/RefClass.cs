﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp9
{
    class RefClass
    {
        public bool Judge(ref int num)
        {
            if (num % 5 == 0)
            {
                return true;
            }
            return false;
        }

        public void Star(out int num1)
        {
            num1 = 6;
        }


    }

        public class OuterClass
        {   
            public class InnerClass
            {
                public string CardId { get; set; }
                public string PassWord { get; set; }
                public void PrintMsg()
                {
                    Console.WriteLine("卡号为：" + CardId);
                    Console.WriteLine("密码为：" + PassWord);
                }
            }

        }


}
