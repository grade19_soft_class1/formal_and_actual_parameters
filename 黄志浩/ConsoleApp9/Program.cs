﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp9
{
    class Program
    {
        //形参实参
        static void Main(string[] args)
        {
            RefClass refClass = new RefClass();
            int a = 10;
            bool result = refClass.Judge(ref a);
            Console.WriteLine("验证结果是："+result);
            Console.WriteLine(a);
            
            int i=80;
            refClass.Star(out i) 
            Console.WriteLine(i);
        //嵌套

            OuterClass.InnerClass innerClass = new OuterClass.InnerClass();
            innerClass.CardId = "666";
            innerClass.PassWord = "123456789";
            innerClass.PrintMsg();
        }
    }
}
