﻿using System;

namespace 实参和形参
{
    struct Astruct
    {
        public int x;
    }
    class Bclass
    {
        public int x;
    }
    class Test
    {
        private static int a;
        public static void Main(string[] args)
        {
            Astruct a = new Astruct();
            modify(ref a);
            Console.WriteLine(a.x);


            Bclass b = new Bclass();
            modify(ref b);
            Console.WriteLine(b.x);
        }
        public static void modify(ref Astruct a)
        {
            a.x++;
        }
        public static void modify(ref Bclass b)
        {
            b.x++;
        }
    }

   /* struct Cstruct
    {
        public int y;
    }
    class Dclass
    {
        public int y;
    }
    class Test1
    {
        private static int c;
        public static void Main(string[] args)
        {
            Cstruct c;
            modify(out c);
            Console.WriteLine(c.y);

            Dclass d;
            modify(out d);
            Console.WriteLine(d.y);


        }
        public static void modify(out Cstruct c)
        {
            c = new Cstruct();
            c.y++;
        }
        public static void modify(out Dclass d)
        {
            d = new Dclass();
            d.y++;
        }
    }*/
}




