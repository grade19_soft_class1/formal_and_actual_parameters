﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Class1
    {
        public void Judge(int num, out bool result)
        {
            if (num % 5 == 0)
            {
                result = true;
            }
            else
            {
                result = false;
            }
        }
    }
}
