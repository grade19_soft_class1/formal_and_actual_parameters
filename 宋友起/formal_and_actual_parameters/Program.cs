﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace formal_and_actual_parameters
{
    class Program
    {
        static void Main(string[] args)
        {
           
           
            Console.WriteLine();
            People xiaoming = new People(10, "AAA");
            Console.WriteLine("调用前：" + xiaoming.ToString());
            People.ChangeAge(xiaoming);
            Console.WriteLine("调用后：" + xiaoming.ToString());

            Console.WriteLine();
            Console.WriteLine("==========================================");



            Console.WriteLine();
            People lanyangyang = new People(12, "BBB");
            Console.WriteLine("调用前：" + lanyangyang.ToString());
            People.ChangeAge(ref lanyangyang);
            Console.WriteLine("调用后：" + lanyangyang.ToString());

            Console.ReadKey();
        }
    }
    
}
