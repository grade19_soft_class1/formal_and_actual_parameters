﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace formal_and_actual_parameters
{
    class People
    {
        public int Age { set; get; }
        public string Name { set; get; }

        public People(int age, string name)
        {
            this.Age = age;
            this.Name = name;
        }
        public override string ToString() => "姓名：" + this.Name + "   年龄：" + this.Age;
        public static void ChangeAge(People people)
        {
            people.ToString();



            People newPeople = new People(30, "X先生");
            people = newPeople; 
            Console.WriteLine(people.ToString());
        }
        public static void ChangeAge(ref People people)
        {
            people.ToString();
            People newPeople = new People(20, "A先生");
            people = newPeople; 
            Console.WriteLine(newPeople.ToString());
        }
    }
}
