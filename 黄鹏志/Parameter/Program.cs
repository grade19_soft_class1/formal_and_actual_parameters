﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.引用类型作为值参数进行传递：在方法里面将参数改变，不会影响到外部的原对象。
            //说明在值参数进行传递的时候，实参和形参都是有各自的内存空间的
            Console.WriteLine("1.引用类型作为值参数进行传递：在方法里面将参数改变，不会影响到外部的原对象。说明在值参数进行传递的时候，实参和形参都是有各自的内存空间的");
            Console.WriteLine();
            People xiaoming = new People(200, "刘小波");
            Console.WriteLine("未调用方法之前：" + xiaoming.ToString());
            People.ChangeAge(xiaoming);
            Console.WriteLine("调用方法之后：" + xiaoming.ToString());
           
            Console.WriteLine();
            Console.WriteLine();



            //2.引用类型作为引用参数进行传递，在方法里面将传进去的参数进行重新初始化，结果会影响到最初外部的原对象。
            //说明在引用参数进行传递的时候，形参其实没有在栈中开辟空间，只是实参的一个别名。
            Console.WriteLine("2.引用类型作为引用参数进行传递，在方法里面将传进去的参数进行重新初始化，结果会影响到最初外部的原对象。说明在引用参数进行传递的时候，形参其实没有在栈中开辟空间，只是实参的一个别名。");
            Console.WriteLine();
            People lanyangyang = new People(100, "黄屁屁");
            Console.WriteLine("未调用方法之前：" + lanyangyang.ToString());
            People.ChangeAge(ref lanyangyang);
            Console.WriteLine("调用方法之后：" + lanyangyang.ToString());
            
            Console.ReadKey();
        }
    }
}
