﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class People
    {
        public int Age { set; get; }
        public string Name { set; get; }

        public People(int age, string name)
        {
            this.Age = age;
            this.Name = name;
        }
        public override string ToString() => "姓名是：" + this.Name + "   年龄是：" + this.Age;
        public static void ChangeAge(People people)
        {
            people.ToString();           
           
            Console.WriteLine("======改变people引用的对象=======");
        
            People newPeople = new People(20, "黄屁屁");
            people = newPeople; //将新对象赋值给参数对象
            Console.WriteLine(people.ToString());
        }
        public  static void ChangeAge(ref People people)
        {
            people.ToString();
            Console.WriteLine("======改变people引用的对象=======");
         
            People newPeople = new People(20, "刘小波");
            people = newPeople; //将新对象赋值给参数对象
            Console.WriteLine(newPeople.ToString());
        }
    }
}
