﻿using System;

namespace ConsoleApp8
{
    class Program
    {
        static void Main(string[] args)
        {
            Method method = new Method();
            int a = 70;
            var x = method.MethodSomething(ref a);
            Console.WriteLine(x);


            method.MethodAnything(out int i);
            Console.WriteLine(i);
        }
    }
}