﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp8
{
    class Method
    {
        public bool MethodSomething(ref int num1)
        {
            if (num1 >= 60)
            {
                return true;
            }
            return false;
        }

        public void MethodAnything (out int num2)
        {
            num2 = 222;
        }
    }
}
