﻿using System;

namespace Homework_
{
    class Program
    {
        static void Main(string[] args)
        {
            Judge judge = new Judge();

            int a = 60;
            var b = judge.JudgeSomething(ref a);

            Console.WriteLine("num="+a);
            Console.WriteLine(judge.JudgeSomething(ref a));
            Console.WriteLine();

            //6546513165
            Gamer g1 = new Gamer();
            g1.Age = " is you ";
            judge.GetSomething(g1);
            Console.WriteLine(g1.Age+" is you");
            Console.WriteLine();

            //456465
            judge.JudgeAngl(out string i);//已经定义i  
            Console.WriteLine(i+" is me");
            Console.WriteLine();

            //5464665
            bool rs;
            judge.Judge_(20, out rs);
            Console.WriteLine(rs);
            Console.WriteLine();

            //嵌套类
            OutrerClass.InnerClass outInner = new OutrerClass.InnerClass();
            outInner.CardId = "6666666669999";
            outInner.PassWord = "8888888888";
            outInner.PrintMsg();


        }
    }
}
