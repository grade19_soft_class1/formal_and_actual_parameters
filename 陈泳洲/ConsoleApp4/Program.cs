﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
                                                                                  //写完这个作业，头发掉了一半       Σ( ° △ °|||)︴

            Console.WriteLine("请输入你的出生日期,格式例如：1949/10/1");          //让用户输入生日，输出年份，月份，日期和年龄, 判断年，月，日是否常规懒得写了

            string birthday = Console.ReadLine();
            int year;                                                             //年份
            int month;                                                            //月份
            int day;                                                              //年龄
            DateTime dt = DateTime.Now;                                           //获取系统年份
            int age = dt.Year;                                                    //给年龄赋值系统年份
            bool bol = true;                                                      //判断是否输出最后的文本的bool值

            Logon.Register(birthday,out year,out month,out day,out bol);          //调用out  分割出年，月，日和异常操作


            Logon.Age(ref age, year);                                             //调用ref和计算年龄
            if (bol)                                                              //判断是否输出最后的文本
            {
                Logon.Speak(year, month, day, age);
            }
            
            Console.ReadKey();



        }
    }
}
