﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    class Outclass
    {
        public void  Solution(int num,out bool result)
        {
            if (num % 5 == 0)
            {
                result = true;
            }
            else
            {
                result=false;
            }
        }
    }
}
