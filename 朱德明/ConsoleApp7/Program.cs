﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            //直接输出结果
            Refclass refclass = new Refclass();
            int a = 60;
            bool result = refclass.Judge(ref a);
            Console.WriteLine("结果是："+result);

            //增加一个参数输出结果
            Outclass outclass = new Outclass();
            bool rs;
            outclass.Solution(13, out rs);
            Console.WriteLine(rs);
        }
    }
}
