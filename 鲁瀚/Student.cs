﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ref_out_params参数区别
{
    public class Student
    {
      
            public int Add(ref int x, int y)
            {
                x = x * y;
                return x;
            }

            public int Chen(int x, int y, out int z)
            {
                z = x * y;
                return z;

            }

    }
}
