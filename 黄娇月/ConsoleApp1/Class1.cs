﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Class1
    {
        static void swap (int x,int y)
        {
            int temp = x;
            x = y;
            y = temp;
        }

        private static void Test2(string[] args)
        {
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("交换前：a={0},b={1},", a, b);
            swap(a, b);
            Console.WriteLine("交换后：a={0}.b={1}", a, b);

        }
    }
}
