﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static double Bigger(double x,double y)
        {
            double maxValue = (x >= y) ? x : y;
            return maxValue;
        }
        static void Main(string[] args)
        {
            double a = Convert.ToDouble(Console.ReadLine());
            double b = Convert.ToDouble(Console.ReadLine());
            double result = Bigger(a,b);
            Console.WriteLine("最大值是：{0}", result);
        }
    }
}
