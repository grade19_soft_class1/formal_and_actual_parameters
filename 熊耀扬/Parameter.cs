﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp4
{
    class Parameter
    {
        public bool ParameterSomething(ref int num1)
        {
            if (num1 % 3 == 0)
            {
                return true;
            }
            else 
            {
                return false;
            }
        }
        public void ParameterAnything(out int num2)
        {
            num2 = 13;
        }
    }
}
