﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Class1
    {
        public bool Build(ref int num1)
        {
            if (num1 % 2 == 0) {
                num1 = 10;
            }
            return false;
        }
        public void Build2(string str)
        {
            Console.WriteLine(str);            
        }
        public void Buildlike(out int num2)
        {
            num2 = 999;
        }
    }
}
