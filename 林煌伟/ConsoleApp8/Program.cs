﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    class Program
    {
        static void Main(string[] args)
        { 
            Student student = new Student();

            Console.WriteLine("你好张三同学你是否要更改学号1.是，2.不是");
            int determine = int.Parse(Console.ReadLine());
            //引用参数
            student.RefClass(ref determine);


            if (determine == 1)
            {
                //形参
                Console.WriteLine("你好请输入你的的学号");
                int StudentCode = int.Parse(Console.ReadLine());

                Console.WriteLine("你的学号是：" + student.StudentCode(StudentCode));
                Console.WriteLine();


                //输出参数
                string name;
                student.OutClass(out name);
                Console.WriteLine();


                //实参
                Console.WriteLine("这是你的新学号");
                student.NewStudentCode();
                Console.WriteLine();

                Console.WriteLine("学号更改完成请记住你的新学号");
                Console.WriteLine();

                Console.WriteLine("旧学号已删除是否退出1.退出");
                int choose = int.Parse(Console.ReadLine());
                Console.WriteLine("正在退出");
            }
            else
            {
                Console.WriteLine("正在退出");
            }
           

        }
    }
}
