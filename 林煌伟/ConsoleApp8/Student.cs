﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    class Student
    {
        //形参
        public int StudentCode(int a)
        {
            return a;
        }


        //实参
        public void NewStudentCode()
        {
            int a= StudentCode(1433223);
            Console.WriteLine(a);
        }


        //引用参数
        public void RefClass(ref int num)
        {
            if (num == 1)
            {
                Console.WriteLine("正在进入");
            }
            else
            {
                Console.WriteLine("你选择不更改学号");
            }
        } 
        
        
        //输出参数
        public void OutClass (out string num2)
        {
            num2 = "张三";
            Console.WriteLine("你好{0}你的学号已更改",num2);
        }
    }
}
