﻿using System;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            Who who = new Who();

            int a = 648;
            var i = who.Two(ref a);

            Console.WriteLine("直接把它干蒙圈："+i);

            who.Second(out int one);
            Console.WriteLine("假的充值："+one);
        }
    }
}
