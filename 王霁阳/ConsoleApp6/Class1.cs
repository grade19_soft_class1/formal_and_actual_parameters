﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Class1
    {
        public bool Judge(ref int num)
        {
            if (num <= 10)
            {
                num = 0;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Print(int num,out bool rq)
        {
            if (num >= 50)
            {
                rq = true;
            }
            else
            {
                rq = false;
            }
        }

    }
}
