﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace work7
{
    class Program
    {
        static void Main(string[] args)
        {   
            int number = 1;//用于ref
            int number1;//用于out
            Method method = new Method();
            method.Method1(ref number);//我变了
            Console.WriteLine(number);
            Console.WriteLine();

            method.Method1(number);//我重载了Method1 但是我没变 因为我没用ref参数 仅仅是地址指向变相同
            Console.WriteLine(number);
            Console.WriteLine();

            method.Method2(number);//我没变 因为我没用ref参数 仅仅是地址指向变相同
            Console.WriteLine(number);
            Console.WriteLine();

            method.Method2(out number1);//我重载了Method2 而且我用了out参数 先把2赋值给number1 然后再加上98 变成100
            Console.WriteLine(number1);
            Console.WriteLine();

            method.Method3(out number1);//我用了out参数 而且我又把100变回了1
            Console.WriteLine(number1);
            Console.WriteLine();
            Console.WriteLine("如果深入思考你会发现 这玩意太复杂了。。。。\n");
        }
    }
}
