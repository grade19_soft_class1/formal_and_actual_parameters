﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace work7
{
    class Method
    {
        public void Method1(ref int refArgument)
        {
            refArgument += 44;
        }

        public void Method1(int refArgument)
        {
            refArgument += 99;
        }

        //        public void Method1(out int refArgument)
        //        {
        //           refArgument = 999;       编译器会报错 因为只有ref和out 区别
        //       }

        public void Method2(int refArgument)
        {
            refArgument += 999;
        }
        public void Method2(out int refArgument1)
        {
            refArgument1 = 2;
            refArgument1 += 98;
        }
        public void Method3(out int refArgument1)
        {
            refArgument1 = 1;
        }

    }
}
