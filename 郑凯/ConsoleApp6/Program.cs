﻿using System;
using System.Runtime.ExceptionServices;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks.Dataflow;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            Special special = new Special();

            int a = 20;
           special.Two(ref a);
           
            Console.WriteLine( a );

            special.Second(out int one);
            Console.WriteLine(one);
        }
    }
    }

