﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入玩家身份:");
            string useName = Console.ReadLine();
            Console.WriteLine("请输入咒语:");
            string usePassword = Console.ReadLine();
            string other;

            bool result = System.Login(useName, usePassword, out other);
            Console.WriteLine("登陆结果{0}", result);
            Console.WriteLine("登陆信息：{0}", other);
            Console.WriteLine();

            
            double gold = 1000;
            System.GoldWashing(ref gold);
            Console.WriteLine("最终你将得到{0}金",gold);
        }
    }
}
