﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework
{
    public class System
    {
        /// <summary>
        /// 输入正确账号密码返回成功着陆，否则返回提示信息
        /// </summary>
        public static bool Login(string name, string password, out string other)
        {
            if (name == "Alice" && password == "123456")
            {
                other = "你已成功着陆淘金岛";
                return true;
            }
            else if (name == "Alice")
            {
                other = "请尝试其它咒语通往淘金岛入口";
                return false;
            }
            else if (password == "123456")
            {
                other = "玩家身份有误，请塑造新身份";
                return false;
            }
            else
            {
                other = "修行不足，请再等一万年";
                return false;
            }
        }

        /// <summary>
        /// 淘金
        /// </summary>
        /// <param name="g">进入淘金岛获得1000金，额外得到600金</param>
        public static void GoldWashing(ref double g)
        {
            g += 600;
        }

    }
}
