﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection.PortableExecutable;
using System.Text;

namespace ConsoleApp4
{
    public class Secondary
    { 
        enum Gender
        {
            男,女
        }
        string Name;
        public Secondary(string name)
        {
            Name = name;
            Console.WriteLine("");
            Console.WriteLine("我的好朋友叫:{0} 他是一个{1}生",Name,Gender.男);
        }

        public double total;
        public double Score(ref double chinese,ref double english)
        {
            total = chinese + english;
            return total;
        }

        public void Age(int age,out int lastYear,out int nextYear)
        {
            lastYear = age - 1;
            nextYear = age + 1;
            Console.WriteLine("去年他{0}岁，今年他{1}岁了，明年他就{2}岁了",lastYear,age,nextYear);
        }



    }


}
