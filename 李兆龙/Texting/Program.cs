using System;

namespace Texting
{
    class Program
    {
        //out
        static void Main(string[] args)
        {
            //out 输出参数
            string str;
            int cc = Test(out str);
            Console.WriteLine(str);
            Console.WriteLine(cc);
            Console.ReadKey();


            //ref 引用参数
            double salary = 3000;
            S(ref salary);
            Console.WriteLine(salary);


            //实参
            Gamer("烬", 2000);
        }

        public static int Test(out string aa)
        {
            aa = "hello";
            int bb = 100;
            return bb;
        }

        //ref
        public static void S(ref double bb)
        {
            bb += 1000;

        }
        //形参
        public static void Gamer(string Jing, int a)
        {
            Console.WriteLine(Jing + ":" + a);
        }



    }
}
